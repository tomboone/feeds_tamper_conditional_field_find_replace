<?php
/*
*
 * @file
 * A Fields Tamper plugin that allows user to select a second field to search for a specific value, and if matched, perform a find and replace within the current field.
 */

$plugin = array(
    'form' => 'feeds_tamper_conditional_field_find_replace_form',
    'callback' => 'feeds_tamper_conditional_field_find_replace_callback',
    'name' => 'Conditional Field Find and Replace',
    'multi' => 'skip',
    'category' => 'Other',
);

function feeds_tamper_conditional_field_find_replace_form($importer, $element_key, $settings) {
    $form = array();
    $form['field_id'] = array(
        '#type' => 'textfield',
        '#title' => 'Conditional Field ID',
        '#default_value' => isset($settings['field_id']) ? $settings['field_id'] : '',
        '#description' => t('The ID of the field you wish to filter feed items by. (If unknown, use the Feeds Debug Tamper and Devel modules to display the full $item array on test import.)'),
        '#required' => TRUE,
    );
    $form['find_condition'] = array(
        '#type' => 'textfield',
        '#title' => t('Text to find at beginning of Conditional Field'),
        '#default_value' => isset($settings['find_condition']) ? $settings['find_condition'] : '',
        '#description' => t('Must match the BEGINNING of the conditional field\'s value'),
        '#required' => TRUE,
    );
    $form['find_current'] = array(
        '#type' => 'textarea',
        '#title' => t('A list of complete field values to find in current field'),
        '#default_value' => isset($settings['find_current']) ? $settings['find_current'] : '',
        '#description' => t('Each line must match a COMPLETE field value'),
        '#required' => TRUE,
    );
    $form['replace'] = array(
        '#type' => 'textfield',
        '#title' => t('New text to replace matched text in current field.'),
        '#default_value' => isset($settings['replace']) ? $settings['replace'] : '',
        '#description' => t('Leave blank to remove the current field value.'),
    );
    $form['invert'] = array(
        '#type' => 'checkbox',
        '#title' => t('Invert current field filter'),
        '#default_value' => isset($settings['invert']) ? $settings['invert'] : FALSE,
        '#description' => t('Inverting the filter will replace items only if the current field does NOT match the searched text.'),
    );
    return $form;
}

function feeds_tamper_conditional_field_find_replace_callback($source, $item_key, $element_key, &$field, $settings) {
    $settings['find_current'] = str_replace("\r", '', $settings['find_current']);
    $settings['word_list'] = explode("\n", $settings['find_current']);
    $settings['word_list'] = array_map('trim', $settings['word_list']);

    if ((strpos($source->items[$item_key][$settings['field_id']], $settings['find_condition']) === 0)) {
        $match = in_array($field, $settings['word_list']);
        if (!empty($settings['invert'])) {
            if ($match == FALSE) {
                $field = $settings['replace'];
            }
        }
        else {
            if ($match == TRUE) {
                $field = $settings['replace'];
            }
        }
    }
}
